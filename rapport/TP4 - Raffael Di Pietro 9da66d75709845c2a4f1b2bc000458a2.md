# TP4 - Raffael Di Pietro

Sujet: [https://fnuttens.gitbook.io/cours-git-1/tp-les-branches](https://fnuttens.gitbook.io/cours-git-1/tp-gitlab)

# Démarrage

### Fork du projet

Il faut cliquer sur le bouton fork en haut à droite de la page

### Clone du projet principal

```jsx
git clone https://gitlab.com/fnuttens/tp-rebase-2021.git
```

### Ajout du remote personal

Ce remote pointe vers notre fork

```jsx
git remote add personal https://gitlab.com/raffaeldp/tp-rebase-2021.git
```

## Votre branche

### Création de l’issue “Modification du readme”

![Untitled](TP4%20-%20Raffael%20Di%20Pietro%209da66d75709845c2a4f1b2bc000458a2/Untitled.png)

### Création de la banche

```jsx
git switch -c 1-customize-readme
```

### Effectuer les changements

```jsx
// Je modifie le readme puis,
git add . 
git commit -m "docs(readme): complete issue 1"
```

### Push dans le remote origin

```jsx
git push origin 1-customize-readme
```

Il est impossible de push dans la branche main du remote origin car je n’ai pas les permissions nécessaires pour le faire.

```jsx
remote: You are not allowed to push code to this project.
fatal: unable to access 'https://gitlab.com/fnuttens/tp-rebase-2021.git/': The requested URL returned error: 403
```

### Push dans mon fork

```jsx
git push personal 1-customize-readme
```

### Merge de mon camarade

J’ai ajouté mon camarade sur gitLab, il m’a laissé un commentaire auquel j’ai répondu puis a merge la merge request.

Visible [ici](https://gitlab.com/raffaeldp/tp-rebase-2021/-/merge_requests/1).

### Que constatez vous ?

Je constate que ma branche a été merger dans ma branche main.

Mon camarade a décoché la case delete source branche, donc elle est toujours présente.

Grâce au nom de la branche qui contenait le numéro de l’issue, elle a été automatiquement fermée lors du merge de la branche.

### Mettre à jour ma branche main en local

```jsx
git pull personal main
```